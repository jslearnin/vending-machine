# Vending Machine

### Background Information:

At Fredmania Vending, your senior Software Engineer created a API for the company's new line of vending machines. The API was created, so that the company can be alerted when product needs to be re-stocked, and so that we can gather accurate sales information.

As a result of our new vending machines, the responsibility of our delivery drivers collecting this information will no longer be necessary.


### Objective:

As a new Software Engineer at the company Fredmania Vending, you are assigned to
create a vending machine prototype to test the company's new vending API. Since
this is a proof-of-concept prototype, you are to write this application in
JavaScript with the jQuery over a web-interface.

This prototype will be presented to our stock holders to promote additional
investing in the new product. The success of this product rests on your
shoulders. (Pressure's on!)

------

## Take-Aways/Research topics:

* Use of AJAX through jQuery
* Variable/Object Caching Techniques
* Exposure to API's interaction development
* JavaScript Timeouts / DateTime
* AJAX request/response handling
* HTTP Status codes
* Error reporting
* Event-handling
* JavaScript/JSON Objects
* JavaScript Arrays
* Javascript Object looping techniques
* JavaScript Object manipulation
* JavaScript Input Dialogs
* Browser Console Log
* Debugging Techniques

**Note:** Research topics will be discussed during scheduled meeting times.
Additionally, please have questions, concerns, and comments prepared prior to
the scheduled meeting time.

## Requirements:

* Code Must meet 40Digits Standards
* JS must use strict standards
* You must have fun!
* Vending machine must keep track of its data locally
* Prototype must work with the 3 test vending machines
* Dialog box to prompt user input
* TBD ...

**Note:** HTML/CSS will be provided.

**Note:** This project is meant to take a lot of time to complete, therefore
must be done at your own pace in order for your learning to be effective. This
project should not be done during normal work hours.


## Bonus:

### Easy

* Change background color of a product to *red* to indicate that the product is *sold out*
* Implement a *shake* mechanic that will output a item from the vending machine. Make sure the machine cheats and notifies the API that a product has been **bought** (Don't tell the investors you did this ;) )

### Intermediate

* Authentication with between the vending machine and the API [^1]
* Allow a driver to manually send statistic data to the API (In case the vending machine has been offline for a long while)

### Advanced

* Create a cookie to save the current state of any particular vending machine,
so that on a page refresh, you can load a previous state. Once the state is loaded, it must syncronize with the API. [^1]
* In the event of internet/wifi outage, the vending machine must keep track of
its data while offline. Once online, the vending machine must synchronize with the API.
While this is similar to the previous bonus, the difference is that the vending machine must be self-aware if it's been disconnected from the internet, in addition to utalizing the methods from the first bonus item. [^1]
* +2,000,000 pts AND graduation from course if you create the API in addition to
the prototype. [^2]

[^1]: Tell the Software Engineer before attempting. Requires additional API development.
[^2]: Additional requirements are necessary. Your API must be able to match **exactly** to the API documentation's output.

------

# API Documentation

TBD ...